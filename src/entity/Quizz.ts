import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  JoinColumn,
  OneToMany,
} from 'typeorm'
import { Question } from './Question'

@Entity()
export class Quizz {
  @PrimaryGeneratedColumn()
  public id: Number

  @Column()
  public title: String

  @OneToMany(() => Question, question => question.quizz)
  @JoinColumn()
  public question: Question
}
