import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  JoinColumn,
  ManyToOne,
} from 'typeorm'
import { Quizz } from './Quizz'

@Entity()
export class Question {
  @PrimaryGeneratedColumn()
  public id: Number

  @Column()
  public question: String

  @Column()
  public image: String

  @JoinColumn()
  @ManyToOne(() => Quizz, quizz => quizz.id)
  public quizz: Quizz
}
